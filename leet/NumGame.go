package main

import "fmt"

/**
描述：小A 和 小B 在玩猜数字。小B 每次从 1, 2, 3 中随机选择一个，小A 每次也从 1, 2, 3 中选择一个猜。
他们一共进行三次这个游戏，请返回 小A 猜对了几次？

示例：
输入：guess = [2,2,3], answer = [3,2,1]
输出：1
解释：小A 只猜对了第二次。
*/

func main() {
	guess := []int{1, 2, 3}
	answer := []int{2, 2, 3}
	count := game(guess, answer)
	fmt.Printf("总共猜对了：%d次", count)
}

func game(guess []int, answer []int) int {
	count := 0
	for i := range guess {
		if guess[i] == answer[i] {
			count++
		}
	}
	return count
}
