package main

import (
	"fmt"
	"strings"
)

func main() {

	J := "aA"
	S := "aAAbbbb"
	count := JewelsInStones(J, S)
	fmt.Printf("宝石个数为：%d", count)
}

func JewelsInStones(J string, S string) int {
	count := 0
	for _, v := range J {
		count += strings.Count(S, string(v))
	}
	return count
}
