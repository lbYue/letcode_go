package main

import "fmt"

/**
描述：给你一个整数数组 nums 。
	如果一组数字 (i,j) 满足 nums[i] == nums[j] 且 i < j ，就可以认为这是一组 好数对 。返回好数对的数目。
示例：
输入：nums = [1,2,3,1,1,3]
输出：4
*/
func main() {
	nums := []int{1, 3, 4, 3, 4}
	count := numIdenticalPairs(nums)
	fmt.Printf("好数对的数目为:%d", count)
}

func numIdenticalPairs(nums []int) int {
	count := 0
	for i := 0; i < len(nums); i++ {
		for j := i + 1; j < len(nums); j++ {
			if nums[i] == nums[j] {
				count++
			}
		}
	}
	return count
}
