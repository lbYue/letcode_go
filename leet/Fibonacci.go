package main

import "fmt"

/**
输出 给定范围内的 斐波那契数列
*/
func main() {
	result := fibonacci(10)
	fmt.Println(result)
}

/**
递归调用
*/
func fibonacci(n int) int {

	if n == 0 {

		return 0
	}
	if n == 1 || n == 2 {
		return 1
	}
	return fibonacci(n-2) + fibonacci(n-1)
}
