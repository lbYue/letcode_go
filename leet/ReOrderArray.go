package main

import "fmt"

/**
描述：给你一个数组 nums ，数组中有 2n 个元素，按 [x1,x2,...,xn,y1,y2,...,yn] 的格式排列。
	请你将数组按 [x1,y1,x2,y2,...,xn,yn] 格式重新排列，返回重排后的数组。

示例:
输入：nums = [2,5,1,3,4,7], n = 3
输出：[2,3,5,4,1,7]
解释：由于 x1=2, x2=5, x3=1, y1=3, y2=4, y3=7 ，所以答案为 [2,3,5,4,1,7]
*/
func main() {
	nums := []int{2, 5, 1, 3, 4, 7}
	result := shuffle(nums, 3)
	fmt.Printf("%v,", result)

}

func shuffle(nums []int, n int) []int {

	//数组长度
	result := make([]int, 0, n*2)
	for i := 0; i < n; i++ {
		result = append(result, nums[i], nums[i+n])
	}
	return result
}
