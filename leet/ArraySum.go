package main

import "fmt"

/**
描述：返回动态的和
示例：
-------------------------
输入：nums = [3,1,2,10,1]
输出：[3,4,6,16,17]
-------------------------
*/
func main() {
	nums := []int{1, 2, 3, 4}
	sum := runningSum(nums)

	for i := range sum {
		fmt.Printf("%d,", sum[i])
	}
}

func runningSum(nums []int) []int {
	for i := range nums {
		if i != 0 {
			nums[i] = nums[i-1] + nums[i]
		}
	}
	return nums
}
