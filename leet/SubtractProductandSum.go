package main

import (
	"fmt"
	"strconv"
)

/**
描述：给你一个整数 n，请你帮忙计算并返回该整数「各位数字之积」与「各位数字之和」的差。
示例：
输入：n = 234
输出：15
	解释：
	各位数之积 = 2 * 3 * 4 = 24
	各位数之和 = 2 + 3 + 4 = 9
	结果 = 24 - 9 = 15
*/

func main() {
	n := 234
	s := subtractProductAndSum(n)
	fmt.Printf("%d", s)
}

func subtractProductAndSum(n int) int {

	s := strconv.Itoa(n)
	subtract := 1
	sum := 0
	for _, v := range s {
		a, err := strconv.Atoi(string(v))
		if err != nil {
			continue
		}
		subtract *= a
		sum += a
	}
	return subtract - sum
}
