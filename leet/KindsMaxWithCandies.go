package main

import "fmt"

/**
描述：给你一个数组 candies 和一个整数 extraCandies ，其中 candies[i] 代表第 i 个孩子拥有的糖果数目。
	对每一个孩子，检查是否存在一种方案，将额外的 extraCandies 个糖果分配给孩子们之后，此孩子有 最多 的糖果。注意，允许有多个孩子同时拥有 最多 的糖果数目。
示例：
输入：candies = [2,3,5,1,3], extraCandies = 3
输出：[true,true,true,false,true]
解释：
孩子 1 有 2 个糖果，如果他得到所有额外的糖果（3个），那么他总共有 5 个糖果，他将成为拥有最多糖果的孩子。
孩子 2 有 3 个糖果，如果他得到至少 2 个额外糖果，那么他将成为拥有最多糖果的孩子。
......
*/

func main() {
	candies := []int{1, 2, 3, 4, 5}
	extraCandies := 2
	withCandies := kidsWithCandies(candies, extraCandies)
	fmt.Printf("%v", withCandies)
}

func kidsWithCandies(candies []int, extraCandies int) []bool {
	n := len(candies)
	maxCandies := 0
	for i := range candies {
		maxCandies = max(maxCandies, candies[i])
	}
	ret := make([]bool, n)
	for i := range candies {
		ret[i] = candies[i]+extraCandies >= maxCandies
	}
	return ret
}

func max(x int, y int) int {
	if x > y {
		return x
	} else {
		return y
	}
}
