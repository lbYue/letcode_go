package main

import "fmt"

/**
描述：给你两个整数，n 和 start 。
数组 nums 定义为：nums[i] = start + 2*i（下标从 0 开始）且 n == nums.length 。
请返回 nums 中所有元素按位异或（XOR）后得到的结果

示例：输入：n = 5, start = 0
输出：8
解释：数组 nums 为 [0, 2, 4, 6, 8]，其中 (0 ^ 2 ^ 4 ^ 6 ^ 8) = 8 。
*/

func main() {
	result := xorOperation(4, 3)
	fmt.Printf("%d", result)

}

func xorOperation(n int, start int) int {
	nums := make([]int, n)
	var result int

	for i := 0; i < n; i++ {
		nums[i] = start + (2 * i)
	}
	for i := range nums {
		if i == 0 {
			result = nums[i]
			continue
		}
		result = result ^ nums[i]
	}
	return result
}
