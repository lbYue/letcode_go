package main

import (
	"fmt"
	"strings"
)

/**
查找字符串数组中的最长公共前缀
输入: ["flower","flow","flight"]
输出: "fl"
*/

func main() {

	strArray := []string{"flow", "flower", "flight"}
	resultStr := longestCommonPrefix(strArray)
	fmt.Println(resultStr)
}

func longestCommonPrefix(strArray []string) string {
	if len(strArray) < 1 {
		return ""
	}

	prefix := strArray[0]
	for _, k := range strArray {
		for strings.Index(k, prefix) != 0 {
			if len(prefix) == 0 {
				return ""
			}
			prefix = prefix[:len(prefix)-1]
		}

	}
	return prefix
}
