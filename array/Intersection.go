package main

import (
	"fmt"
)

/**
求数组交集
输入: nums1 = [4,9,5], nums2 = [9,4,9,8,4]

输出: [4,9]
*/
func main() {
	nums1 := []int{4, 2, 3}
	nums2 := []int{2, 4, 5, 3}

	result := intersection(nums1, nums2)

	fmt.Println(result)
}

func intersection(nums1 []int, nums2 []int) []int {

	m0 := map[int]int{}
	for _, v := range nums1 {
		fmt.Println(m0[v])
		m0[v] += 1
		fmt.Println(m0[v])
	}

	k := 0
	for _, v := range nums2 {
		if m0[v] > 0 {
			m0[v] -= 1
			nums2[k] = v
			k++
		}

	}
	return nums2[0:k]

}
